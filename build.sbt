name := "deploymanager"

organizationName := "br.com.ingenieux"

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.10.3"

libraryDependencies += "com.amazonaws" % "aws-java-sdk" % "1.6.5"

resolvers ++= Seq("snapshots"     at "http://oss.sonatype.org/content/repositories/snapshots",
                  "staging"       at "http://oss.sonatype.org/content/repositories/staging",
                  "releases"      at "http://oss.sonatype.org/content/repositories/releases",
                  "twttr"         at "http://maven.twttr.com/"
                 )

scalacOptions ++= Seq("-deprecation", "-unchecked", "-explaintypes")

seq(filterSettings: _*)

seq(com.typesafe.sbt.SbtNativePackager.packagerSettings: _*)

packageArchetype.java_application

libraryDependencies ++= {
  Seq(
      "commons-lang"               % "commons-lang"     % "2.6",
      "br.com.ingenieux.cloudy"    % "cloudy-awseb"     % "0.0.4",
      "net.codingwell"             % "scala-guice_2.10" % "4.0.0-beta",
      "com.amazonaws"              % "aws-java-sdk"     % "1.6.5",
      "com.twitter"                % "finatra"          % "1.4.1",
      "org.scalatest"              % "scalatest_2.10.0" % "2.0.M5" % "test"
  )
}

// exportJars := true

// seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

mainClass in (Compile, run) := Some("br.com.ingenieux.deploymanager.Server")