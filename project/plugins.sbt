//resolvers += Resolver.url(
//  "scalasbt-artifactoryonline",
//  new URL("http://scalasbt.artifactoryonline.com/scalasbt/sbt-plugin-releases/")
//)(Resolver.ivyStylePatterns)

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "0.6.4")

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.5.2")

addSbtPlugin("com.github.sdb" % "xsbt-filter" % "0.4")

// addSbtPlugin("org.scala-sbt.plugins" % "sbt-onejar" % "0.8")
