package br.com.ingenieux.deploymanager.model

import scala.annotation.meta.beanGetter
import com.amazonaws.services.dynamodbv2.datamodeling._

object ScalaMappings {
  type HashKey = DynamoDBHashKey @beanGetter

  type Attribute = DynamoDBAttribute @beanGetter

  type Table = DynamoDBTable
}
