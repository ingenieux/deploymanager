package br.com.ingenieux.deploymanager.model

import scala.beans.BeanProperty
import org.apache.commons.lang.builder.{ToStringStyle, ToStringBuilder}
import br.com.ingenieux.deploymanager.model.ScalaMappings._
import java.util.{Collections, Set, TreeSet}
import scala.collection.JavaConversions

@Table(tableName = "user")
class User(var _id: String, var _sshKeys: Set[String]) {
  @BeanProperty  @HashKey(attributeName = "id")
  var id = _id

  @BeanProperty @Attribute(attributeName = "ssh_keys")
  var sshKeys: Set[String] = new TreeSet[String](_sshKeys)

  def this() = this(null, Collections.emptySet[String]())

  def this(_id: String, _sshKeys: String*) = this(_id, new TreeSet(JavaConversions.seqAsJavaList(_sshKeys)))

  override def toString =
    new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
      append(this.id).
      append(this.sshKeys).
      toString
}
