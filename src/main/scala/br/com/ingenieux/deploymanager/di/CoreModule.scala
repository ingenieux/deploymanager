package br.com.ingenieux.deploymanager.di

import net.codingwell.scalaguice.ScalaModule
import com.google.inject.{Inject, Provides, AbstractModule}
import com.amazonaws.services.dynamodbv2.datamodeling.{DynamoDBMapper, DynamoDBMapperConfig}
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.TableNameOverride
import br.com.ingenieux.cloudy.awseb.di.BaseAWSModule
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB

class CoreModule(prefix: String) extends AbstractModule with ScalaModule {
  def this() = this(null)

  val ddbConfig = new DynamoDBMapperConfig(TableNameOverride.withTableNamePrefix(if (null == prefix) "" else s"$prefix-"))

  def configure() {
    install(new BaseAWSModule().withRegion("us-east-1"))
  }

  @Provides
  @Inject
  def getDynamoDBMapper(ddb: AmazonDynamoDB) = new DynamoDBMapper(ddb, ddbConfig)
}
