package br.com.ingenieux.deploymanager

import com.twitter.finatra._
import com.twitter.finatra.ContentType._

object Server extends FinatraServer {
  class Service extends Controller {
    get("/") { req =>
      render.plain("Hello, World!").toFuture
    }
  }

  val service = new Service()

  register(service)
}