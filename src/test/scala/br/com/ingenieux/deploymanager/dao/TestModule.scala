package br.com.ingenieux.deploymanager.dao

import net.codingwell.scalaguice.ScalaModule
import com.google.inject.{Provides, AbstractModule, Inject}
import br.com.ingenieux.cloudy.awseb.di.BaseAWSModule
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import br.com.ingenieux.deploymanager.di.CoreModule

class TestModule extends CoreModule("deploytest") {
}
