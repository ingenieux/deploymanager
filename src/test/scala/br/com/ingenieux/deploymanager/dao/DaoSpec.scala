package br.com.ingenieux.deploymanager.dao

import org.scalatest._
import com.google.inject.Guice
import javax.inject.Inject
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper
import br.com.ingenieux.deploymanager.model.User
import scala.collection.{JavaConversions, mutable}
import java.util

class DaoSpec extends FunSuite {
  val KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIEAttTcZ1HI1Z/1rSlek9o1mp8aRxhWv7JtXmSThpZGXliRm+h3W/Q/5osNvk4vYQV7oFHV+UjJB+JTL57igV9q3bx2Kxx1gZ1BZhEigTDCsulV4FBETfucvOpbu7jWSgCI/MSSPMNzOVz055VdVpPdEwB+3CUo0Cx24AiSvRUbaYc="

  @Inject
  var mapper: DynamoDBMapper = _

  Guice.createInjector(new TestModule()).injectMembers(this)

  test("An user is persisted") {
    val user = new User("aldrinleal", KEY)

    mapper.save(user)
  }

}
